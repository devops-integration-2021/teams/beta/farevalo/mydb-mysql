#PASOS A SEGUIR PARA CREAR

cd mysql-dev/

docker build -t mysql-dev:1 .

docker images

docker ps -a

docker run -it -d -p 4000:3306 -e MYSQL_ROOT_PASSWORD=root --name mydb mysql-dev:1 mysqld --lower_case_table_names=1
